import React from "react";
import {connect} from "react-redux"
import CalculatorResults from "./CalculatorResults";
import {
    calculateAmount,
    changeAmount,
    changeCalculateType,
    decreaseGoalByDate,
    increaseGoalByDate
} from "../redux/actions";

const CalculatorForm = ({changeAmount, calculateAmount, changeCalculateType, increaseGoalByDate, decreaseGoalByDate, isByTotal, inputtedAmount, goalBy}) => {
    const beautifyAmount = (amount) => String(amount).replace(/(\d)(?=(\d{3})+(\D|$))/g, '$1,')
    const setAmountNumber = (amount) => Number(amount.replace(/[a-zA-Zа-яА-Я.,;:&()*%#-]/g, "").replace(/ /g, ""))
    const beautifyDate = (date) => {
        return new Date(date).toLocaleString('en-EN', {month: 'long'}) + ", " + new Date(date).getFullYear()
    }

    const handleCalculationType = (e) => {
        changeCalculateType(e.target.checked);
    };

    const handleAmount = (e) => {
        changeAmount(setAmountNumber(e.target.value));
    };

    const increaseDate = () => {
        increaseGoalByDate();
    };

    const decreaseDate = () => {
        decreaseGoalByDate();
    };

    const calculateTotal = () => {
        let d1 = new Date()
        let d2 = new Date(goalBy)
        let months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth();
        months += d2.getMonth();
        months = months <= 0 ? 0 : months
        calculateAmount(months, isByTotal ? Math.round((inputtedAmount / months) * 100) / 100 : inputtedAmount * months)
    }

    return (
        <form className="flex-col j-start a-start">
            <div className="flex a-center j-start">
                <label className="switch">
                    <input checked={isByTotal} onChange={(e) => {
                        handleCalculationType(e)
                        calculateTotal()
                    }} type="checkbox"/>
                    <span className="slider round"/>
                </label>
                <p className="custom-label">{
                    isByTotal
                        ? "Calculate by total amount"
                        : "Calculate by monthly saving"
                }</p>
            </div>
            <div className="flex-col a-start j-start w-100">
                <label htmlFor="totalAmount" className="custom-label">{
                    isByTotal
                        ? "Total amount"
                        : "Monthly amount"
                }</label>
                <div className="custom-input-container w-100">
                    <div className="custom-input__icon flex a-center j-center">$</div>
                    <input id="totalAmount" className="custom-input w-100" name="amount" type="text"
                           value={beautifyAmount(inputtedAmount)} onChange={(e) => {
                        handleAmount(e)
                        calculateTotal()
                    }}/>
                </div>
            </div>
            <div className="flex-col a-start j-start w-100">
                <label htmlFor="goalBy" className="custom-label">Reach goal by</label>
                <div className="custom-input-container w-100">
                    <div className="custom-input__icon custom-input__button flex a-center j-center"
                         onClick={() => {
                             decreaseDate()
                             calculateTotal()
                         }}>{"<"}</div>
                    <input id="goalBy" className="custom-input w-100" name="goalBy" type="month" readOnly
                           value={beautifyDate(goalBy)}/>
                    <div
                        className="custom-input__icon custom-input__button custom-input__icon_last flex a-center j-center"
                        onClick={() => {
                            increaseDate()
                            calculateTotal()
                        }}>{">"}</div>
                </div>
            </div>
            <CalculatorResults beautifyAmount={beautifyAmount} beautifyDate={beautifyDate}/>
            <button type="submit" className="custom-button w-100">Finish</button>
        </form>
    )
}

const mapDispatchToProps = {
    changeCalculateType,
    changeAmount,
    increaseGoalByDate,
    decreaseGoalByDate,
    calculateAmount
};

const mapStateToProps = state => ({
    isByTotal: state.isByTotal,
    inputtedAmount: state.inputtedAmount,
    goalBy: state.goalBy,
});

export default connect(mapStateToProps, mapDispatchToProps)(CalculatorForm);