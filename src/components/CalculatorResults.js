import React from "react";
import {connect} from "react-redux"

const CalculatorResults = ({isByTotal, inputtedAmount, goalBy, calculatedAmount, monthCount, beautifyAmount, beautifyDate}) => {
    return (
        <div className="calculator__result-container flex-col a-center j-between w-100">
            <div className="calculator-result__amount flex a-center j-between w-100">
                <p className="calculator-result__amount_p">{
                    isByTotal
                        ? "Monthly amount"
                        : "Total amount"
                }</p>
                <h3 className="calculator-result__amount_h3">${beautifyAmount(calculatedAmount)}</h3>
            </div>
            <div className="calculator-result__stuff-info w-100">
                {
                    isByTotal
                        ? <p className="calculator-result__stuff-info_p">You are
                            planning <strong>{monthCount}</strong> monthly deposits to reach
                            your <strong>${beautifyAmount(inputtedAmount)}</strong> goal
                            by <strong>{beautifyDate(goalBy)}</strong>.</p>
                        : <p className="calculator-result__stuff-info_p">You are
                            saving <strong>${beautifyAmount(inputtedAmount)}</strong> monthly to
                            save <strong>${beautifyAmount(calculatedAmount)}</strong> by <strong>{beautifyDate(goalBy)}</strong>.</p>
                }
            </div>
        </div>
    )
};

const mapStateToProps = state => ({
    isByTotal: state.isByTotal,
    inputtedAmount: state.inputtedAmount,
    goalBy: state.goalBy,
    calculatedAmount: state.calculatedAmount,
    monthCount: state.monthCount
});

export default connect(mapStateToProps, null)(CalculatorResults)