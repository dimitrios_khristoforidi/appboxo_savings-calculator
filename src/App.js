import React from 'react';
import ChevronLeft from "./stuff/icons/chevron-left";
import CalculatorForm from "./components/CalculatorForm";

function App() {
    return (
        <div className="container">
            <div className="header w-100">
                <div className="header__icon-link"><ChevronLeft/></div>
                <h2 className="header__h2">Let’s plan your saving goal</h2>
            </div>
            <div className="calculator-wrapper w-100">
                <h1 className="calculator__h1">Savings calculator</h1>
                <CalculatorForm/>
            </div>
        </div>
    );
}

export default App;
