import {
    CALCULATE_TOTAL,
    CHANGE_AMOUNT,
    CHANGE_CALCULATE_TYPE,
    DECREASE_GOAL_DATE,
    INCREASE_GOAL_DATE
} from "./types";

const initialState = {
    isByTotal: false,
    inputtedAmount: 0,
    goalBy: Date.now(),
    calculatedAmount: 0,
    monthCount: 0
};

export const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_CALCULATE_TYPE:
            return {...state, isByTotal: action.payload}
        case CHANGE_AMOUNT:
            return {...state, inputtedAmount: action.payload}
        case INCREASE_GOAL_DATE:
            return {...state, goalBy: state.goalBy + 2592000000}
        case DECREASE_GOAL_DATE:
            return {...state, goalBy: state.goalBy - 2592000000}
        case CALCULATE_TOTAL:
            return {...state, monthCount: action.month, calculatedAmount: action.amount}
        default:
            return state;
    }
};