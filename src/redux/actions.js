import {
    CALCULATE_TOTAL,
    CHANGE_AMOUNT,
    CHANGE_CALCULATE_TYPE,
    DECREASE_GOAL_DATE,
    INCREASE_GOAL_DATE
} from "./types";

export const changeCalculateType = (type) => ({
    type: CHANGE_CALCULATE_TYPE,
    payload: type,
});

export const changeAmount = (amount) => ({
    type: CHANGE_AMOUNT,
    payload: amount,
});

export const increaseGoalByDate = () => ({
    type: INCREASE_GOAL_DATE,
});

export const decreaseGoalByDate = () => ({
    type: DECREASE_GOAL_DATE,
});

export const calculateAmount = (month, amount) => ({
    type: CALCULATE_TOTAL,
    month: month,
    amount: amount,
});